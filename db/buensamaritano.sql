--
-- File generated with SQLiteStudio v3.2.1 on Sun Jul 19 22:05:03 2020
--
-- Text encoding used: UTF-8
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: assistances
CREATE TABLE assistances (
    id                  INTEGER PRIMARY KEY UNIQUE NOT NULL, 
    assistance_type_id  INTEGER NOT NULL, 
    created_at          VARCHAR NOT NULL, 
    updated_at          VARCHAR, 
    deleted_at          VARCHAR, 
    beneficiary_id      INTEGER NOT NULL REFERENCES beneficiaries (id), 
    churh_id            INTEGER REFERENCES churchs (id), 
    delivered_at        VARCHAR NOT NULL, 
    latitude            VARCHAR, 
    longitude           VARCHAR,
    image               VARCHAR
);

-- Table: assistances_type
CREATE TABLE assistances_type (
    id                 INTEGER PRIMARY KEY
                               UNIQUE
                               NOT NULL,
    name VARCHAR NOT NULL,
    created_at         VARCHAR NOT NULL,
    updated_at         VARCHAR,
    deleted_at         VARCHAR
);

-- Table: beneficiaries
CREATE TABLE beneficiaries (
    id                 INTEGER PRIMARY KEY
                               UNIQUE
                               NOT NULL,
    document_number    VARCHAR NOT NULL,
    name               VARCHAR NOT NULL,
    phone_number       VARCHAR,
    is_landline           INT,  
    created_at         VARCHAR NOT NULL,
    updated_at         VARCHAR,
    deleted_at         VARCHAR
);

-- Table: churchs
CREATE TABLE churchs (
    id                 INTEGER PRIMARY KEY
                               UNIQUE
                               NOT NULL,
    name VARCHAR NOT NULL,
    created_at         VARCHAR NOT NULL,
    updated_at         VARCHAR,
    deleted_at         VARCHAR,
    longitude VARCHAR,
    latitude VARCHAR 
);

COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
