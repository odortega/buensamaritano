import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ComponentsModule } from '../../components/components.module';

import { AssistancePage } from './assistance.page';

//PROVIDERS
import {AssistanceProvider } from '../../providers/assistance/assistance';
import {AssistanceTypeProvider } from '../../providers/assistance_type/assistance_type';
import {BeneficiaryProvider } from '../../providers/beneficiary/beneficiary';
import {HelperProvider} from '../../providers/helper/helper';


const routes: Routes = [
  {
    path: '',
    component: AssistancePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    ComponentsModule
  ],
  declarations: [AssistancePage],
  providers: [
    BeneficiaryProvider,
    AssistanceTypeProvider,
    AssistanceProvider,
    HelperProvider,
  ]
})
export class AssistancePageModule {}
