import { Component } from '@angular/core';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { Router, NavigationExtras } from '@angular/router';
import { MenuController, ToastController } from '@ionic/angular';

//INTERFACES
import { IBeneficiary } from '../../interfaces/beneficiary.interface';
import { IAssistance } from '../../interfaces/assistance.interface';
import { IAssistanceType } from '../../interfaces/assistance_type.interface';
import { IChurch } from '../../interfaces/church.interface';
import { IUser } from '../../interfaces/user.interface';

//PROVIDERS
import { AuthProvider } from '../../providers/auth/auth';
import { HelperProvider } from '../../providers/helper/helper';
import {AssistanceProvider } from '../../providers/assistance/assistance';
import {AssistanceTypeProvider } from '../../providers/assistance_type/assistance_type';
import { BeneficiaryProvider } from '../../providers/beneficiary/beneficiary';



@Component({
  selector: 'app-assistance',
  templateUrl: './assistance.page.html',
  styleUrls: [
    './styles/assistance.page.scss'
  ]
})
export class AssistancePage {
  assistanceForm: FormGroup;

  assistanceTypesList: Array<any> = [];

  userLogged: IUser = null;

  assistance: IAssistance = {
    id: null,
    created_at: null,
    updated_at: null,
    delivered_at: null,
    assistance_type_id: null,
    beneficiary_id: null,
    church_id: null,
    document_number: null,
    phone_number: null,
    is_landline: null,
    latitude: null,
    longitude: null
  };

  assistanceType: IAssistanceType = {
    id: null,
    name: null
  };

  church: IChurch = {
    id: null,
    name: null
  };

  validation_messages = {
    'churh_id': [
      { type: 'required', message: 'Parroquia es requerida.' },
      { type: 'pattern', message: 'Ingrese una Parroquia válida.' }
    ],
    'document_number': [
      { type: 'required', message: 'Número documento es requerido.' },
      { type: 'minlength', message: 'Número documento debe contener al menos 5 caracteres.' }
    ],
    'assistance_type_id': [
      { type: 'required', message: 'Tipo de asistencia es requerida.' },
      { type: 'minlength', message: 'Tipo de asistencia debe contener al menos 7 caracteres.' }
    ],
  };

  constructor(
    public router: Router,
    public menu: MenuController,
    public toastController: ToastController,
    private assistanceProvider: AssistanceProvider,
    private assistanceTypeProvider: AssistanceTypeProvider,
    private authProvider: AuthProvider,
    private beneficiaryProvider: BeneficiaryProvider,
    private helper: HelperProvider,
  ) {
    this.assistanceForm = new FormGroup({
      'churh_id': new FormControl('1', Validators.compose([
        Validators.required,
        //Validators.pattern('/^[a-zA-Z ]*$/')
      ])),
      'assistance_type_id': new FormControl('1', Validators.compose([
        Validators.required,
        //Validators.pattern('/^[a-zA-Z ]*$/')
      ])),
      'document_number': new FormControl('32455058', Validators.compose([
        Validators.minLength(5),
        Validators.required
      ])),
      'phone_number': new FormControl('', Validators.compose([
        Validators.minLength(7),
        Validators.required
      ])),
      'is_landline': new FormControl('', Validators.compose([]))
    });
  }

  // Disable side menu for this page
  async  ionViewDidEnter(){
    this.menu.enable(false);
    const isLogged = await this.authProvider.checkSession();
    if (isLogged) {
      this.userLogged = this.authProvider.userLogged;
      this.getAssistanceTypes();
    }
  }


  // Restore to default when leaving this page
  ionViewDidLeave(): void {
    this.menu.enable(true);
  }


  async getAssistanceTypes() {
    try {
      let res = await this.assistanceTypeProvider.listAssistanceTypes().toPromise();
      var dataProperty = 'data';
      if(dataProperty in res){
        this.assistanceTypesList = res[dataProperty];
      }
    } catch (e) {
      const messageError = 'No hemos podido consultar los tipos de asistencia. Por favor intentalo más tarde.'
      this.helper.apiError(e, messageError)
    }

  }

  doViewAssistances(): void {
    console.log('do View Assistances');
    this.router.navigate(['app/beneficiaries/beneficiaries-list']);
  }


  /** Redirect to assistancetypes-list page sending IAssistanceType 
   * @param assistanceType?: IAssistanceType
  */
 goToAssistanceTypesList(assistanceType?: IAssistanceType): void {
  const navigationExtras: NavigationExtras = {
    queryParams: {
      assistanceType: JSON.stringify(assistanceType)
    }
  };
  this.router.navigate(['/app/assistancetypes/assistancetypes-list'], navigationExtras);
}



  /** Redirect to churchs-list page sending IChurch 
   * @param church?: IAssistanceType
  */
 goToChurchsList(church?: IChurch): void {
  const navigationExtras: NavigationExtras = {
    queryParams: {
      church: JSON.stringify(church)
    }
  };
  this.router.navigate(['/app/churchs/churchs-list'], navigationExtras);
}

  async presentToastWithOptions() {
    const toast = await this.toastController.create({
      header: 'Última asistencia',
      message: '13/02/2020, Alimentación, Santa Teresita',
      position: 'middle',
      color: 'dark',
      buttons: [
        {
          side: 'start',
          icon: 'help-buoy',
          text: '',
          handler: () => {
            console.log('Favorite clicked');
          }
        }, {
          text: 'Cerrar',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    toast.present();
  }

}
