import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-assistances',
  templateUrl: './assistances.page.html',
  styleUrls: [
    './styles/assistances.page.scss',
    './styles/assistances.shell.scss'
  ]
})

export class AssistancesPage implements OnInit {
  assistances: any;
  showFilter  = false;
  constructor(private route: ActivatedRoute,
    public router: Router
  ) { }

  ngOnInit(): void {
    if (this.route && this.route.data) {
      this.route.data.subscribe(resolvedData => {
        const dataSource = resolvedData['data'];
        if (dataSource) {
          dataSource.source.subscribe(pageData => {
            if (pageData) {
              this.assistances = pageData;
            }
          });
        }
      });
    }
  }


  doGoDetail(): void {
    console.log('do View Assistances');
    this.router.navigate(['/app/assistances/assistances-detail']);
  }

}
