import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';

import { AssistancesService } from '../assistances.service';

@Injectable()
export class AssistancesResolver implements Resolve<any> {

  constructor(private assistancesService: AssistancesService) { }

  resolve() {
    // Base Observable (where we get data from)
    const dataObservable = this.assistancesService.getData();
    return { source: dataObservable };
  }
}
