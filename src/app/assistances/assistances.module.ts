import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { ComponentsModule } from '../../components/components.module';

import { AssistancesPage } from './assistances-list/assistances.page';
import { AssistancesResolver } from '../assistances/assistances.resolver';
import { AssistancesService } from '../assistances/assistances.service';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ComponentsModule,
    RouterModule.forChild([
      {
        path: '',
        component: AssistancesPage,
        resolve: {
          data: AssistancesResolver
        }
      }
    ])
  ],
  declarations: [ AssistancesPage ],
  providers: [
    AssistancesResolver,
    AssistancesService
  ]
})
export class AssistancesPageModule {}
