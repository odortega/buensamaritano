import { ShellModel } from '../../shell/data-store';

export class AssistanceTypesModel extends ShellModel {
  beneficiaries: Array<{
    id: number,
    name: string
  }> = [
    {
      id: 1,
      name: 'Alimentación'
    },
    {
      id: 2,
      name: 'Salud'
    },
    {
      id: 3,
      name: 'Educación'
    }
  ];

  constructor() {
    super();
  }
}
