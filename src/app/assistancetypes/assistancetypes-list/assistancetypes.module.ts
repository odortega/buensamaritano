import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { IonicModule } from '@ionic/angular';

import { ComponentsModule } from '../../components/components.module';

import { AssistanceTypesService } from '../assistancetypes.service';
import { AssistanceTypesPage } from './assistancetypes.page';
import { AssistanceTypesResolver } from './assistancetypes.resolver';


//PROVIDERS
import {AssistanceTypeProvider} from '../../providers/assistance_type/assistance_type';
import {HelperProvider} from '../../providers/helper/helper';


const routes: Routes = [
  {
    path: '',
    component: AssistanceTypesPage,
    resolve: {
      data: AssistanceTypesResolver
    }
  }
];

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ComponentsModule,
    RouterModule.forChild(routes),
    HttpClientModule
  ],
  declarations: [AssistanceTypesPage],
  providers: [
    AssistanceTypesResolver,
    AssistanceTypesService,
    AssistanceTypeProvider,
    HelperProvider
  ]
})
export class AssistanceTypesPageModule {}
