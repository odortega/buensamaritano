
import { Component, OnInit, HostBinding, Injector } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { Subscription } from 'rxjs';

import { IResolvedRouteData, ResolverHelper } from '../../utils/resolver-helper';
import { AssistanceTypesModel } from './assistancetypes.model';
import { MenuController } from '@ionic/angular';


//INTERFACES
import { IAssistanceType } from '../../interfaces/assistance_type.interface';
import { IUser } from '../../interfaces/user.interface';

//PROVIDERS
import { AuthProvider } from '../../providers/auth/auth';
import { HelperProvider } from '../../providers/helper/helper';
import { AssistanceTypeProvider } from '../../providers/assistance_type/assistance_type';

@Component({
  selector: 'app-assistancetypes',
  templateUrl: './assistancetypes.page.html',
  styleUrls: [
    './styles/assistancetypes.page.scss',
    './styles/assistancetypes.shell.scss',
    './styles/assistancetypes.ios.scss',
    './styles/assistancetypes.md.scss'
  ]
})
export class AssistanceTypesPage implements OnInit {

  userLogged: IUser = null

  // Gather all component subscription in one place. Can be one Subscription or multiple (chained using the Subscription.add() method)
  subscriptions: Subscription;

  data: AssistanceTypesModel;

  segmentValue = 'assistancetypes';
  assistanceTypesList: Array<any> = [];
  searchQuery = '';
  showFilters = false;
  private authProvider: AuthProvider;

  @HostBinding('class.is-shell') get isShell() {
    return (this.data && this.data.isShell) ? true : false;
  }

  constructor(private route: ActivatedRoute,
    public router: Router,
    public menu: MenuController,
    private assistanceTypeProvider: AssistanceTypeProvider,
    private helper: HelperProvider,
    private injector: Injector
  ) {
    //TO AVOID CIRCULAR DEPENDENCIES
    this.authProvider = this.injector.get(AuthProvider);
  }

  async ngOnInit() {
    await this.menu.enable(false);
  }

  async ionViewWillEnter() {
  }


  async ionViewDidEnter() {
    const isLogged = await this.authProvider.checkSession();
    if (isLogged) {
      this.userLogged = this.authProvider.userLogged;
      this.getAssistanceTypes();
    }
  }


  // Restore to default when leaving this page
  async ionViewDidLeave() {
    await this.menu.enable(true);
  }

  async getAssistanceTypes() {
    try {
      let res = await this.assistanceTypeProvider.listAssistanceTypes().toPromise();
      var dataProperty = 'data';
      if (dataProperty in res) {
        this.data = res[dataProperty];
        this.assistanceTypesList = res[dataProperty];
      }
    } catch (e) {
      const messageError = 'No hemos podido consultar los Tipos de Asistencia. Por favor intentalo más tarde.'
      this.helper.apiError(e, messageError)
    }

  }

  searchList(): void {
    const query = (this.searchQuery && this.searchQuery !== null) ? this.searchQuery : '';

    if (this.segmentValue === 'assistancetypes') {
      this.assistanceTypesList = this.filterList(this.data, query);
    }
  }

  filterList(list, query): Array<any> {
    return list.filter(item => item.name.toLowerCase().includes(query.toLowerCase()));
  }

  // NOTE: Ionic only calls ngOnDestroy if the page was popped (ex: when navigating back)
  // Since ngOnDestroy might not fire when you navigate from the current page, use ionViewWillLeave to cleanup Subscriptions
  ionViewWillLeave(): void {
    // console.log('TravelListingPage [ionViewWillLeave]');
    //this.subscriptions.unsubscribe();
  }

  /** Redirect to assistance-detail page sending IAssistanceType 
   * @param assistanceType?: IAssistanceType
  */
  goToBackPage(assistanceType?: IAssistanceType): void {
    const navigationExtras: NavigationExtras = {
      queryParams: {
        assistanceType: JSON.stringify(assistanceType)
      }
    };
    this.router.navigate(['/app/assistances/assistances-detail'], navigationExtras);
  }
}
