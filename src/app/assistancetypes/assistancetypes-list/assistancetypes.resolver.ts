import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';

import { AssistanceTypesService } from '../assistancetypes.service';

@Injectable()
export class AssistanceTypesResolver implements Resolve<any> {

  constructor(private assistanceTypesService: AssistanceTypesService) { }

  resolve() {
    // Base Observable (where we get data from)
    const dataObservable = this.assistanceTypesService.getData();
    return { source: dataObservable };
  }
}
