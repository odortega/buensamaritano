import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ComponentsModule } from '../../components/components.module';

import { BeneficiaryPage } from './beneficiary.page';

//PROVIDERS
import {BeneficiaryProvider} from '../../providers/beneficiary/beneficiary';
import {HelperProvider} from '../../providers/helper/helper';


const routes: Routes = [
  {
    path: '',
    component: BeneficiaryPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    ComponentsModule
  ],
  declarations: [BeneficiaryPage],
  providers: [
    BeneficiaryProvider,
    HelperProvider,
  ]

})
export class BeneficiaryPageModule {}
