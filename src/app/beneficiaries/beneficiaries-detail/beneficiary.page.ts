import { Component, Injector } from '@angular/core';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MenuController } from '@ionic/angular';


//INTERFACES
import { IBeneficiary } from '../../interfaces/beneficiary.interface';

//PROVIDERS
import { AuthProvider } from '../../providers/auth/auth';
import { HelperProvider } from '../../providers/helper/helper';
import { BeneficiaryProvider } from '../../providers/beneficiary/beneficiary';



@Component({
  selector: 'app-beneficiary',
  templateUrl: './beneficiary.page.html',
  styleUrls: [
    './styles/beneficiary.page.scss'
  ]
})
export class BeneficiaryPage {
  beneficiaryForm: FormGroup;

  validation_messages = {
    'name': [
      { type: 'required', message: 'Nombre completo es requerido.' },
      { type: 'pattern', message: 'Ingrese un Nombre completo válido.' }
    ],
    'document_number': [
      { type: 'required', message: 'Número documento es requerido.' },
      { type: 'minlength', message: 'Número documento debe contener al menos 5 caracteres.' }
    ],
    'phone_number': [
      { type: 'required', message: 'Número telefónico es requerido.' },
      { type: 'minlength', message: 'Número telefónico debe contener al menos 7 caracteres.' }
    ],
  };


  beneficiary: IBeneficiary = {
    id: null,
    name: '',
    document_number: null,
    phone_number: null,
    is_landline: false,
  };

  private authProvider: AuthProvider;


  constructor(
    public router: Router,
    private activatedRoute: ActivatedRoute,
    public menu: MenuController,
    private beneficiaryProvider: BeneficiaryProvider,
    private helper: HelperProvider,
    private injector: Injector
  ) {
    //TO AVOID CIRCULAR DEPENDENCIES
    this.authProvider = this.injector.get(AuthProvider);
    this.beneficiaryForm = new FormGroup({
      'name': new FormControl('', Validators.compose([
        Validators.required,
        //Validators.pattern('/^[a-zA-Z ]*$/')
      ])),
      'document_number': new FormControl('', Validators.compose([
        Validators.minLength(5),
        Validators.required
      ])),
      'phone_number': new FormControl('', Validators.compose([
        Validators.minLength(7),
        Validators.required
      ])),
      'is_landline': new FormControl('', Validators.compose([]))
    });
  }


  async ngOnInit() {
    this.menu.enable(false);
  }

  // Disable side menu for this page
  async  ionViewDidEnter(){
    const isLogged = await this.authProvider.checkSession();
    if (isLogged) {
      this.setBeneficiary();
    }
  }

  // Restore to default when leaving this page
  ionViewDidLeave(): void {
    this.menu.enable(true);
  }

  
  doViewAssistances(): void {
    console.log('do View Assistances');
    this.router.navigate(['app/assistances/assistances-list']);
  }


  /**
 * Sets to current IBeneficiary object from navParams
 * or from local storage recurrent's cart
*/
  async setBeneficiary() {
    this.activatedRoute.queryParams.subscribe((params) => {
      if (params && params.beneficiary) {
        this.beneficiary = <IBeneficiary>JSON.parse(params.beneficiary)
      }
    });
  }

  saveBeneficiary(beneficiary: IBeneficiary) {
    const data = {
      name: beneficiary.name,
      document_number: beneficiary.document_number,
      phone_number: beneficiary.phone_number,
      is_landline: (beneficiary.is_landline) ? 1 : beneficiary.is_landline,
    };

    if (!beneficiary.id) {
      this.createBeneficiary(data);
    }
    else {
      data['id'] = beneficiary.id
      this.updateBeneficiary(data);
    }
  }

  createBeneficiary(data: any) {
    this.beneficiaryProvider.createBeneficiary(data).subscribe(
      (data: any) => {
        if (data.msg) {
          this.beneficiary.id = data.data;
          this.helper.hideLoading();
          this.helper.alertHeader(`<p>Beneficiario creado con exito.</p>`);
          console.log(data);
        }
      },
      (e) => {
        this.helper.hideLoading();
        this.helper.apiError(e);
      }
    );
  }

  updateBeneficiary(data: any) {
    this.beneficiaryProvider.updateBeneficiary(data).subscribe(
      (data: any) => {
        if (data.msg) {
          this.beneficiary.id = data.data;
          this.helper.hideLoading();
          this.helper.alertHeader(`<p>Beneficiario actualizado con exito.</p>`);
          console.log(data);
        }
      },
      (e) => {
        this.helper.hideLoading();
        this.helper.apiError(e);
      }
    );
  }

}
