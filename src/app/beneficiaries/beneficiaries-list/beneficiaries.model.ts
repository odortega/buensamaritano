import { ShellModel } from '../../shell/data-store';

export class BeneficiariesModel extends ShellModel {
  beneficiaries: Array<{
    id: string,
    document_number: string,
    name: string,
    phone_number: string,
    is_landline: number}> = [
      {
        id: '',
        document_number: '',
        name: '',
        phone_number: '',
        is_landline: 0
      },
      {
        id: '',
        document_number: '',
        name: '',
        phone_number: '',
        is_landline: 0
      },
      {
        id: '',
        document_number: '',
        name: '',
        phone_number: '',
        is_landline: 0
      },
      {
        id: '',
        document_number: '',
        name: '',
        phone_number: '',
        is_landline: 0
      }
          ];

  constructor() {
    super();
  }
}
