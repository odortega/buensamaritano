import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { IonicModule } from '@ionic/angular';

import { ComponentsModule } from '../../components/components.module';

import { BeneficiariesService } from '../beneficiaries.service';
import { BeneficiariesPage } from './beneficiaries.page';
import { BeneficiariesResolver } from './beneficiaries.resolver';


//PROVIDERS
import {BeneficiaryProvider} from '../../providers/beneficiary/beneficiary';
import {HelperProvider} from '../../providers/helper/helper';


const routes: Routes = [
  {
    path: '',
    component: BeneficiariesPage,
    resolve: {
      data: BeneficiariesResolver
    }
  }
];

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ComponentsModule,
    RouterModule.forChild(routes),
    HttpClientModule
  ],
  declarations: [BeneficiariesPage],
  providers: [
    BeneficiariesResolver,
    BeneficiariesService,
    BeneficiaryProvider,
    HelperProvider
  ]
})
export class BeneficiariesPageModule {}
