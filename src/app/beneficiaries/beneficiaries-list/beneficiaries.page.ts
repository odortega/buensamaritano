
import { Component, OnInit, HostBinding, Injector } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { Subscription } from 'rxjs';

import { IResolvedRouteData, ResolverHelper } from '../../utils/resolver-helper';
import { BeneficiariesModel } from './beneficiaries.model';

//INTERFACES
import { IBeneficiary } from '../../interfaces/beneficiary.interface';
import { IUser } from '../../interfaces/user.interface';

//PROVIDERS
import { AuthProvider } from '../../providers/auth/auth';
import { HelperProvider } from '../../providers/helper/helper';
import { BeneficiaryProvider } from '../../providers/beneficiary/beneficiary';

@Component({
  selector: 'app-beneficiaries',
  templateUrl: './beneficiaries.page.html',
  styleUrls: [
    './styles/beneficiaries.page.scss',
    './styles/beneficiaries.shell.scss',
    './styles/beneficiaries.ios.scss',
    './styles/beneficiaries.md.scss'
  ]
})
export class BeneficiariesPage implements OnInit {

  userLogged: IUser = null

  // Gather all component subscription in one place. Can be one Subscription or multiple (chained using the Subscription.add() method)
  subscriptions: Subscription;

  data: BeneficiariesModel;

  segmentValue = 'beneficiaries';
  beneficiariesList: Array<any> = [];
  searchQuery = '';
  showFilters = false;
  private authProvider: AuthProvider;

  @HostBinding('class.is-shell') get isShell() {
    return (this.data && this.data.isShell) ? true : false;
  }

  constructor(private route: ActivatedRoute,
    public router: Router,
    private beneficiaryProvider: BeneficiaryProvider,
    private helper: HelperProvider,
    private injector: Injector
  ) {
    //TO AVOID CIRCULAR DEPENDENCIES
    this.authProvider = this.injector.get(AuthProvider);
  }

  async ngOnInit() {
  }

  async ionViewWillEnter() {
  }


  async  ionViewDidEnter(){
    const isLogged = await this.authProvider.checkSession();
    if (isLogged) {
      this.userLogged = this.authProvider.userLogged;
      this.getBeneficiaries();
    }
  }

  async getBeneficiaries() {
    try {
      let res = await this.beneficiaryProvider.listBeneficiaries().toPromise();
      var dataProperty = 'data';
      if(dataProperty in res){
        this.data = res[dataProperty];
        this.beneficiariesList = res[dataProperty];
      }
    } catch (e) {
      const messageError = 'No hemos podido consultar los beneficiarios. Por favor intentalo más tarde.'
      this.helper.apiError(e, messageError)
    }

  }

  searchList(): void {
    const query = (this.searchQuery && this.searchQuery !== null) ? this.searchQuery : '';

    if (this.segmentValue === 'beneficiaries') {
      this.beneficiariesList = this.filterList(this.data, query);
    }
  }

  filterList(list, query): Array<any> {
    return list.filter(item => item.name.toLowerCase().includes(query.toLowerCase()));
  }

  // NOTE: Ionic only calls ngOnDestroy if the page was popped (ex: when navigating back)
  // Since ngOnDestroy might not fire when you navigate from the current page, use ionViewWillLeave to cleanup Subscriptions
  ionViewWillLeave(): void {
    // console.log('TravelListingPage [ionViewWillLeave]');
    //this.subscriptions.unsubscribe();
  }

  /** Redirect to beneficiary-detail page sending IBeneficiary 
   * @param beneficiary?: IBeneficiary
  */
  goBeneficiaryDetail(beneficiary?: IBeneficiary): void {
    const navigationExtras: NavigationExtras = {
      queryParams: {
        beneficiary: JSON.stringify(beneficiary)
      }
    };
    this.router.navigate(['/app/beneficiaries/beneficiaries-detail'], navigationExtras);
  }
}
