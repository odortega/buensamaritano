import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs';

import { DataStore } from '../../shell/data-store';
import { BeneficiariesService } from '../beneficiaries.service';
import { BeneficiariesModel } from './beneficiaries.model';

@Injectable()
export class BeneficiariesResolver implements Resolve<DataStore<BeneficiariesModel>> {

  constructor(private beneficiariesService: BeneficiariesService) { }

  resolve(): DataStore<BeneficiariesModel> {
    const dataSource: Observable<BeneficiariesModel> = this.beneficiariesService.getBeneficiariesDataSource();
    const dataStore: DataStore<BeneficiariesModel> = this.beneficiariesService.getBeneficiariesStore(dataSource);

    return dataStore;
  }
}
