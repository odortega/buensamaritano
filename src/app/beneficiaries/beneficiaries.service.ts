import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { DataStore } from '../shell/data-store';
import { BeneficiariesModel } from './beneficiaries-list/beneficiaries.model';

@Injectable()
export class BeneficiariesService {
  private beneficiariesDataStore: DataStore<BeneficiariesModel>;

  constructor(private http: HttpClient) { }


  public getBeneficiariesDataSource(): Observable<BeneficiariesModel> {
    return this.http.get<BeneficiariesModel>('./assets/sample-data/beneficiaries/beneficiaries.json')
    .pipe(
      map(
        (data: BeneficiariesModel) => {
          // Note: HttpClient cannot know how to instantiate a class for the returned data
          // We need to properly cast types from json data
          const beneficiaries = new BeneficiariesModel();

          // The Object.assign() method copies all enumerable own properties from one or more source objects to a target object.
          // Note: If you have non-enummerable properties, you can try a spread operator instead. friends = {...data};
          // (see: https://scotch.io/bar-talk/copying-objects-in-javascript#toc-using-spread-elements-)
          Object.assign(beneficiaries, data);

          return beneficiaries;
        }
      )
    );
  }

  public getBeneficiariesStore(dataSource: Observable<BeneficiariesModel>): DataStore<BeneficiariesModel> {
    // Use cache if available
    if (!this.beneficiariesDataStore) {
      // Initialize the model specifying that it is a shell model
      const shellModel: BeneficiariesModel = new BeneficiariesModel();
      this.beneficiariesDataStore = new DataStore(shellModel);
      // Trigger the loading mechanism (with shell) in the dataStore
      this.beneficiariesDataStore.load(dataSource);
    }
    return this.beneficiariesDataStore;
  }

}
