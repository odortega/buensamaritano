import { ShellModel } from '../../shell/data-store';

export class ChurchsModel extends ShellModel {
  beneficiaries: Array<{
    id: number,
    name: string
  }> = [
    {
      id: 1,
      name: 'Parroquia 1'
    },
    {
      id: 2,
      name: 'Parroquia 2'
    },
    {
      id: 3,
      name: 'Parroquia 3'
    }
  ];

  constructor() {
    super();
  }
}
