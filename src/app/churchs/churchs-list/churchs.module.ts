import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { IonicModule } from '@ionic/angular';

import { ComponentsModule } from '../../components/components.module';

import { ChurchsService } from '../churchs.service';
import { ChurchsPage } from './churchs.page';
import { ChurchsResolver } from './churchs.resolver';


//PROVIDERS
import {ChurchProvider} from '../../providers/church/church';
import {HelperProvider} from '../../providers/helper/helper';


const routes: Routes = [
  {
    path: '',
    component: ChurchsPage,
    resolve: {
      data: ChurchsResolver
    }
  }
];

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ComponentsModule,
    RouterModule.forChild(routes),
    HttpClientModule
  ],
  declarations: [ChurchsPage],
  providers: [
    ChurchsResolver,
    ChurchsService,
    ChurchProvider,
    HelperProvider
  ]
})
export class ChurchsPageModule {}
