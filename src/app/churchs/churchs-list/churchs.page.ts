
import { Component, OnInit, HostBinding, Injector } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { Subscription } from 'rxjs';

import { IResolvedRouteData, ResolverHelper } from '../../utils/resolver-helper';
import { ChurchsModel } from './churchs.model';
import { MenuController } from '@ionic/angular';


//INTERFACES
import { IChurch } from '../../interfaces/church.interface';
import { IUser } from '../../interfaces/user.interface';

//PROVIDERS
import { AuthProvider } from '../../providers/auth/auth';
import { HelperProvider } from '../../providers/helper/helper';
import { ChurchProvider } from '../../providers/church/church';

@Component({
  selector: 'app-churchs',
  templateUrl: './churchs.page.html',
  styleUrls: [
    './styles/churchs.page.scss',
    './styles/churchs.shell.scss',
    './styles/churchs.ios.scss',
    './styles/churchs.md.scss'
  ]
})
export class ChurchsPage implements OnInit {

  userLogged: IUser = null

  // Gather all component subscription in one place. Can be one Subscription or multiple (chained using the Subscription.add() method)
  subscriptions: Subscription;

  data: ChurchsModel;

  segmentValue = 'churchs';
  churchsList: Array<any> = [];
  searchQuery = '';
  showFilters = false;
  private authProvider: AuthProvider;

  @HostBinding('class.is-shell') get isShell() {
    return (this.data && this.data.isShell) ? true : false;
  }

  constructor(private route: ActivatedRoute,
    public router: Router,
    public menu: MenuController,
    private churchProvider: ChurchProvider,
    private helper: HelperProvider,
    private injector: Injector
  ) {
    //TO AVOID CIRCULAR DEPENDENCIES
    this.authProvider = this.injector.get(AuthProvider);
  }

  async ngOnInit() {
    this.menu.enable(false);
  }

  async ionViewWillEnter() {
  }


  async ionViewDidEnter() {
    const isLogged = await this.authProvider.checkSession();
    if (isLogged) {
      this.userLogged = this.authProvider.userLogged;
      this.getChurchs();
    }
  }


    // Restore to default when leaving this page
    ionViewDidLeave(): void {
      this.menu.enable(true);
    }
  
  async getChurchs() {
    try {
      let res = await this.churchProvider.listChurchs().toPromise();
      var dataProperty = 'data';
      if (dataProperty in res) {
        this.data = res[dataProperty];
        this.churchsList = res[dataProperty];
      }
    } catch (e) {
      const messageError = 'No hemos podido consultar las Parroquias. Por favor intentalo más tarde.'
      this.helper.apiError(e, messageError)
    }

  }

  searchList(): void {
    const query = (this.searchQuery && this.searchQuery !== null) ? this.searchQuery : '';

    if (this.segmentValue === 'churchs') {
      this.churchsList = this.filterList(this.data, query);
    }
  }

  filterList(list, query): Array<any> {
    return list.filter(item => item.name.toLowerCase().includes(query.toLowerCase()));
  }

  // NOTE: Ionic only calls ngOnDestroy if the page was popped (ex: when navigating back)
  // Since ngOnDestroy might not fire when you navigate from the current page, use ionViewWillLeave to cleanup Subscriptions
  ionViewWillLeave(): void {
    // console.log('TravelListingPage [ionViewWillLeave]');
    //this.subscriptions.unsubscribe();
  }

  /** Redirect to assistance-detail page sending IAssistanceType 
   * @param church?: IAssistanceType
  */
  goToBackPage(church?: IChurch): void {
    const navigationExtras: NavigationExtras = {
      queryParams: {
        church: JSON.stringify(church)
      }
    };
    this.router.navigate(['/app/assistances/assistances-detail'], navigationExtras);
  }
}
