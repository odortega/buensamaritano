import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';

import { ChurchsService } from '../churchs.service';

@Injectable()
export class ChurchsResolver implements Resolve<any> {

  constructor(private churchsService: ChurchsService) { }

  resolve() {
    // Base Observable (where we get data from)
    const dataObservable = this.churchsService.getData();
    return { source: dataObservable };
  }
}
