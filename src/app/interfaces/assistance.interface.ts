export interface IAssistance{
    id: number
    created_at: string 
    updated_at: string 
    delivered_at: string
    assistance_type_id: number
    beneficiary_id: number
    church_id: number
    document_number: number
    phone_number: number
    is_landline: boolean
    latitude: number
    longitude: number
}
