export interface IBeneficiary{
    id: number
    name: string
    document_number: number
    phone_number: number
    is_landline: boolean
}