export interface IUser{
    id: number
    document_type_id: number
    document_id: string
    name: string
    email: string
    phone: string
    last_connection: string
    image: string
}
