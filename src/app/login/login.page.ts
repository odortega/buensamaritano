import { Component } from '@angular/core';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { MenuController } from '@ionic/angular';
import { HelperProvider } from '../providers/helper/helper';
import { AuthProvider } from '../providers/auth/auth';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: [
    './styles/login.page.scss'
  ]
})
export class LoginPage {
  loginForm: FormGroup;
  email: string = null;
  password: string = null;

  validation_messages = {
    'email': [
      { type: 'required', message: 'Email es requerido.' },
      { type: 'pattern', message: 'Ingrese un Email válido.' }
    ],
    'password': [
      { type: 'required', message: 'Contraseña es requerida.' },
      { type: 'minlength', message: 'Contraseña debe contener al menos 5 caracteres.' }
    ]
  };

  constructor(
    public router: Router,
    public menu: MenuController,
    private helper: HelperProvider, 
    private authProvider: AuthProvider,
  ) {
    this.loginForm = new FormGroup({
      'email': new FormControl('test@test.com', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      'password': new FormControl('123456', Validators.compose([
        Validators.minLength(5),
        Validators.required
      ]))
    });
  }

  // Disable side menu for this page
  ionViewDidEnter(): void {
    this.menu.enable(false);
    this.logout();
  }

  // Restore to default when leaving this page
  ionViewDidLeave(): void {
    this.menu.enable(true);
  }


  login(): void {
    this.authProvider.login(this.email, this.password).subscribe( 
      (data: any) => {
        this.setSessionLoginData(data);
      }, 
      (e) => {
        if (e.error && e.error.errors && e.error.errors.length > 0) {
          this.helper.apiError(e, e.error.errors[0]);
        } else {
          this.helper.apiError(e, "Por favor intente más tarde.");
        }
      }
    );
    console.log('login()');
    this.router.navigate(['app/beneficiaries/beneficiaries-list']);
  }

  async logout() {
    try {
      await this.authProvider.logout();
    } catch (e) {
      this.helper.apiError(e, 'No ha sido posible cerrar tu sesión, intentalo de nuevo más tarde');
    }
  }


  private async setSessionLoginData(data) {
    try {
      await this.authProvider.setSession(data);
    } catch (e) {
      this.helper.alertHeader("No se ha podido iniciar sesión");
    }
  }

}
