import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';


/*
  Generated class for the ApiProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ApiProvider {
  options: any;
  headers: object;
  constructor(private http: HttpClient) {}

  post(endPoint, data, token?) {
    let headers: HttpHeaders = new HttpHeaders({
      'Accept':  'application/json',
      'Content-Type':  'application/json',
    });
    if(token){
      headers  = new HttpHeaders({
        'Accept':  'application/json',
        'Content-Type':  'application/json',
        'Authorization': `Bearer ${token}`,
      });
    }
    const httpOptions = {
      headers: headers
    };
    return this.http.post(environment.API_URL + endPoint, data, httpOptions);
  }

  get(query, token?) {
    let headers: HttpHeaders = new HttpHeaders({
      'Accept':  'application/json',
      'Content-Type':  'application/x-www-form-urlencoded; charset=UTF-8',
    });
    if(token){
      headers  = new HttpHeaders({
        'Accept':  'application/json',
        'Content-Type':  'application/x-www-form-urlencoded; charset=UTF-8',
        'Authorization': `Bearer ${token}`,
      });
    }

    const httpOptions = {
      headers: headers
    };
    return this.http.get(environment.API_URL + query, httpOptions);
  }
}