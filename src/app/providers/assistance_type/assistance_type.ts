import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiProvider } from '../api/api';
import { AuthProvider } from '../auth/auth';

/*
  Generated class for the OrderProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AssistanceTypeProvider {
  private endPoint = 'auth/assistance_types/';
  constructor(
    public http: HttpClient, 
    private api: ApiProvider, 
    private authProvider: AuthProvider
  ) {}

  listAssistanceTypes() {
    const token = this.authProvider.apiToken;
    return this.api.get(this.endPoint + 'listAssistanceTypes', token);
  }

  createAssistance(data) {
    const token = this.authProvider.apiToken;
    return this.api.post(this.endPoint + 'createAssistanceType', data, token);
  }

  updateAssistance(data) {
    const token = this.authProvider.apiToken;
    return this.api.post(this.endPoint + 'updateAssistanceType', data, token);
  }

  deleteAssistance(data){
    return this.api.post(this.endPoint + 'deleteAssistanceType', data);
  }

}
