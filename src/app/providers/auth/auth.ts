import { Injectable, Injector } from '@angular/core';
import { ApiProvider } from '../api/api';
import { IUser } from '../../interfaces/user.interface';
import { BehaviorSubject} from 'rxjs';
import { UserProvider } from '../../providers/user/user';
import { HelperProvider } from '../../providers/helper/helper';

/*
  Generated class for the AuthProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AuthProvider {
  private endPoint = 'auth/';
  private _isUserLogged = new BehaviorSubject<boolean>(false);
  isUserLogged$ = this._isUserLogged.asObservable();
  isLogged: boolean = false;

  userLogged: IUser = null;
  apiToken: string = null;
  userProvider: any;
  private helper: HelperProvider;

  constructor(private api: ApiProvider, private injector: Injector) {
    this.helper = this.injector.get(HelperProvider);
    setTimeout(() => {
      this.userProvider = this.injector.get(UserProvider);
    });
  }

  private set isUserLogged(value: boolean) {
    if (this.isLogged !== value) {
      this._isUserLogged.next(value);
      this.isLogged = value;
      if(!this.isLogged) this.userLogged = null;
    }
  }

  private get isUserLogged():boolean {
    return this._isUserLogged.getValue();
  }

  login(email: string, password: string) {
    const data = {
      email,
      password: password
    };

    return this.api.post(this.endPoint + 'login', data);
  }

  register(email: string, password: string) {
    const data = {
      email,
      password
    };

    return this.api.post(this.endPoint + 'register', data);
  }

  // CHECK SESSION STATUS
  async checkSession() {
    const userLogged  = await this.helper.getFromStorage('user');
    const token       = await this.helper.getFromStorage('token');
    if (userLogged && token) {
      this.userLogged = <IUser> JSON.parse(userLogged);
      this.apiToken = token;
      this.isUserLogged = true;
      this.isLogged = true;
      return true;
    } else {
      this.apiToken = null;
      this.isUserLogged = false;
      this.isLogged = false;
      return false;
    }
  }

  async logout() {
    sessionStorage.clear();
    this.isUserLogged = false;
    this.userLogged = null;
    this.apiToken = null;
    this.isLogged = false;
    await this.helper.clearStorage();
  }

  recoverPassword(email){
    return this.api.post(this.endPoint + 'passwordrecover', {
      email
    });
  }

  // SET SESSION VARS
  async setSession(apiResponse): Promise<any> {
    const user = <IUser> apiResponse.user;
    const token = apiResponse.token;

    await this.helper.setInStorage('user', JSON.stringify(user));
    await this.helper.setInStorage('token', token);

    await this.checkSession();
  }

  async setEditInfo(user: IUser) {
    this.userLogged = { ...user };

    return await this.helper.setInStorage('user', JSON.stringify(this.userLogged));
  }
}