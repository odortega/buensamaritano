import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiProvider } from '../api/api';
import { AuthProvider } from '../auth/auth';

/*
  Generated class for the OrderProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class BeneficiaryProvider {
  private endPoint = 'auth/beneficiaries/';
  constructor(
    public http: HttpClient, 
    private api: ApiProvider, 
    private authProvider: AuthProvider
  ) {}

  listBeneficiaries() {
    const token = this.authProvider.apiToken;
    return this.api.get(this.endPoint + 'listBeneficiaries', token);
  }

  createBeneficiary(data) {
    const token = this.authProvider.apiToken;
    return this.api.post(this.endPoint + 'createBeneficiary', data, token);
  }

  updateBeneficiary(data) {
    const token = this.authProvider.apiToken;
    return this.api.post(this.endPoint + 'updateBeneficiary', data, token);
  }

  deleteBeneficiary(data){
    return this.api.post(this.endPoint + 'deleteBeneficiary', data);
  }

}
