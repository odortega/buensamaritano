import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiProvider } from '../api/api';
import { AuthProvider } from '../auth/auth';

/*
  Generated class for the OrderProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ChurchProvider {
  private endPoint = 'auth/churchs/';
  constructor(
    public http: HttpClient, 
    private api: ApiProvider, 
    private authProvider: AuthProvider
  ) {}

  listChurchs() {
    const token = this.authProvider.apiToken;
    return this.api.get(this.endPoint + 'listChurchs', token);
  }

  createAssistance(data) {
    const token = this.authProvider.apiToken;
    return this.api.post(this.endPoint + 'createChurch', data, token);
  }

  updateAssistance(data) {
    const token = this.authProvider.apiToken;
    return this.api.post(this.endPoint + 'updateChurch', data, token);
  }

  deleteAssistance(data){
    return this.api.post(this.endPoint + 'deleteChurch', data);
  }

}
