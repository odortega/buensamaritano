import { HttpClient } from '@angular/common/http';
import { AlertController, LoadingController } from '@ionic/angular';
import { Injectable, Injector } from '@angular/core';
import { environment } from '../../../environments/environment';
import { Storage } from '@ionic/storage';


@Injectable()
export class HelperProvider {
  loading: any;
  minCart: boolean = true;
  scriptsLoaded = [];
  loadingPresent: boolean = false;
  private cartProvider: any;

  constructor(
    private alertCtrl: AlertController,
    private loadingCtrl: LoadingController,
    private storage: Storage,
    private injector: Injector,
  ) {
    this.minCart = true;
  }

  async showLoading(text = "Cargando...") {
    if (!this.loadingPresent) {
      let loadingOpts = {
        content: text};
      //TODO custom loading text  
      this.loading = this.loadingCtrl.create();
      await this.loading.present();
      this.loadingPresent = true;
    }
  }

  hideLoading() {
    if (this.loadingPresent) {
      this.loading.dismiss();
      this.loadingPresent = false;
    }
  }


async  alert(title, text, button = null, autoHide = false, cssClass = null, onOkClick = null, options = null) {
    const alertConfig = {
      title: title,
      message: text,
      cssClass,
    }

    const alert =  await this.alertCtrl.create({...options, ...alertConfig});
    await alert.present();
  }

  alertHeader(text, button = null, autoHide = false, textTitle = null, onOkClick = null, options = null) {
    this.alert(
      `${textTitle ? `<h3>${textTitle}</h3>` : ''}<div class='alert-title'></div>`,
      text,
      button,
      autoHide,
      'alert-header',
      onOkClick,
      options
    );
  }

  apiError(response, customMessage = null, reportToSentry = true) {
    let message = 'Fallas en la comunicación';
    if (!customMessage) {
      if (
        response.error &&
        response.error.errors &&
        response.error.errors.length > 0
      ) {
        message = response.error.errors[0]
      }
    } else {
      message = customMessage;
    }

    this.alertHeader(message, null, false, 'Tenemos fallas en la comunicación');

  }

  async confirmAlert(text, callback, cancelText = 'Regresar', confirmText = 'Continuar') {
    let alert = await this.alertCtrl.create({
      message: text,
      cssClass: 'alert-header',
      buttons: [
        {
          text: cancelText,
          role: "cancel",
          handler: () => {
            return callback(false);
          }
        },
        {
          text: confirmText,
          handler: () => {
            return callback(true);
          }
        }
      ]
    });
    await alert.present();
  }

  cleanString(str) {
    const searchFor = "áéíóúÁÉÍÓÚàèìòùÀÈÌÒÙâêîôûÂÊÎÔÛäëïöüÄËÏÖÜñÑÜü._";
    const replaceWith = "aeiouAEIOUaeiouAEIOUaeiouAEIOUaeiouAEIOUnnuu--";
    let ret = "";

    for (let i = 0; i < str.length; i++) {
      let char = str.charAt(i);
      let match = false;

      let j = 0;
      for (j; j < searchFor.length; j++) {
        if (char === searchFor.charAt(j)) {
          match = true;
          break;
        }
      }

      if (match) {
        ret += replaceWith.charAt(j);
      } else {
        ret += str.charAt(i);
      }
    }
    return ret;
  }

  remoteImg(img) {
    return environment.IMAGES_URL + img;
  }

  dayStr(numDay) {
    switch (parseInt(numDay)) {
      case 1:
        return "Lunes";
      case 2:
        return "Martes";
      case 3:
        return "Miércoles";
      case 4:
        return "Jueves";
      case 5:
        return "Viernes";
      case 6:
        return "Sábado";
      case 7:
        return "Domingo";
      case 99:
        return "Días festivos";
    }
  }


  loadScripts(scripts) {
    let promises: any[] = [];
    scripts.forEach(script => promises.push(this.loadScript(script)));
    return Promise.all(promises);
  }

  private loadScript(src) {
    return new Promise((resolve, reject) => {
      //resolve if already loaded
      if (this.scriptsLoaded.indexOf(src) > -1) {
        resolve({ script: src, loaded: true, status: "Already Loaded" });
      } else {
        //load script
        let script: any = document.createElement("script");
        script.type = "text/javascript";
        script.src = src;
        if (script.readyState) {
          //IE
          script.onreadystatechange = () => {
            if (
              script.readyState === "loaded" ||
              script.readyState === "complete"
            ) {
              script.onreadystatechange = null;
              // this.scriptsLoaded.push(src);
              resolve({ script: src, loaded: true, status: "Loaded" });
            }
          };
        } else {
          //Others
          script.onload = () => {
            // this.scriptsLoaded.push(src);
            resolve({ script: src, loaded: true, status: "Loaded" });
          };
        }
        script.onerror = (error: any) =>
          resolve({ script: src, loaded: false, status: "Loaded" });
        document.getElementsByTagName("head")[0].appendChild(script);
      }
    });
  }


  // STORAGE
  getFromStorage(key: string): Promise<any> {
    return this.storage.get(key).then(data => {
      if (data === "null") return null;
      return data;
    });
  }

  setInStorage(key: string, data: any): Promise<any> {
    return this.storage.set(key, data);
  }

  clearStorage(): Promise<any> {
    return this.storage.clear();
  }

  removeInStorage(key): Promise<any> {
    return this.storage.remove(key);
  }

  /**
   * Use local storage when the information is not sensible and don't want to be removed when user logouts
   * @param key
   */
  getFromLocalStorage(key: string) {
    if (typeof (Storage) !== 'undefined') {
      return window.localStorage.getItem(key);
    }
    return null;
  }

  /**
   * Use local storage when the information is not sensible and don't want to be removed when user logouts
   * @param key
   * @param data
   */
  setInLocalStorage(key: string, data: any) {
    if (typeof (Storage) !== 'undefined') {
      return window.localStorage.setItem(key, data);
    }
    return null;
  }

  
  

  diasEntreFechas(inicio) {
    let fin = this.fechahoy();
    let fechaInicio = new Date(inicio).getTime();
    let fechaFin = new Date(fin).getTime();
    let diff = fechaFin - fechaInicio;
    return Math.round(Number((diff / (1000 * 60 * 60 * 24))));
  }

  fechahoy() {
    let hoy = new Date();
    let dd: any = hoy.getDate();
    let mm: any = hoy.getMonth() + 1;
    let yyyy = hoy.getFullYear();
    let hh: any = hoy.getHours();
    let ii: any = hoy.getMinutes();
    let ss: any = hoy.getSeconds();
    dd = (dd < 10) ? "0" + dd : dd;
    mm = (mm < 10) ? "0" + mm : mm;
    hh = (hh < 10) ? "0" + hh : hh;
    ii = (ii < 10) ? "0" + ii : ii;
    ss = (ss < 10) ? "0" + ss : ss;
    return yyyy + "-" + mm + "-" + dd + " " + hh + ":" + ii + ":" + ss;
  }

  fechaFormat(date) {
    let dd: any = date.getDate();
    let mm: any = date.getMonth() + 1;
    let yyyy = date.getFullYear();
    let hh: any = date.getHours();
    let ii: any = date.getMinutes();
    let ss: any = date.getSeconds();
    dd = (dd < 10) ? "0" + dd : dd;
    mm = (mm < 10) ? "0" + mm : mm;
    hh = (hh < 10) ? "0" + hh : hh;
    ii = (ii < 10) ? "0" + ii : ii;
    ss = (ss < 10) ? "0" + ss : ss;
    return yyyy + "-" + mm + "-" + dd + " " + hh + ":" + ii + ":" + ss;
  }

  fechaNoMayorA24(date) {
    let newDate = date.split(" ");
    let ultimaHoraDelDia = newDate[0] + " 23:59:59";

    let HoraActual = this.fechahoy();
    let fechaUltimaHoraDelDia = new Date(ultimaHoraDelDia).getTime();
    let fechaHoraActual = new Date(HoraActual).getTime();
    return (fechaUltimaHoraDelDia > fechaHoraActual)
  }

  objectToQueryParams(params, keys = [], isArray = false) {
    const p = Object.keys(params)
      .map(key => {
        let val = params[key];

        if (
          "[object Object]" === Object.prototype.toString.call(val) ||
          Array.isArray(val)
        ) {
          if (Array.isArray(params)) {
            keys.push("");
          } else {
            keys.push(key);
          }
          return this.objectToQueryParams(val, keys, Array.isArray(val));
        } else {
          let tKey = key;

          if (keys.length > 0) {
            const tKeys = isArray ? keys : [...keys, key];
            tKey = tKeys.reduce((str, k) => {
              return "" === str ? k : `${str}[${k}]`;
            }, "");
          }
          if (isArray) {
            return `${tKey}[]=${encodeURIComponent(val)}`;
          } else {
            return `${tKey}=${encodeURIComponent(val)}`;
          }
        }
      })
      .join("&");

    keys.pop();
    return p;
  }

  isInputEmpty(value, isEmail = false) {
    if (isEmail) {
      const expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
      if (!expr.test(value)) {
        return true;
      }
    }

    return !value || value === '' || value.code === "0" || (Array.isArray(value) && value.length === 0);
  }

}
