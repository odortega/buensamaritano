import { Injectable, Injector } from '@angular/core';
import { AuthProvider } from '../auth/auth';
import { ApiProvider } from '../api/api';
import { HelperProvider } from '../../providers/helper/helper';


@Injectable()
export class UserProvider {
  private endPoint = "login/";
  apiToken: any;
  private authProvider: any;
  private helper: any;

  constructor(
    //private authProvider: AuthProvider,
    private injector: Injector,
    private api: ApiProvider
  ) {
    this.helper = this.injector.get(HelperProvider);
    setTimeout(() => {
      this.authProvider = this.injector.get(AuthProvider);
      this.apiToken = this.authProvider.apiToken;
    });
  }
}
