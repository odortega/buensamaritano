import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: '',
    component: TabsPage,
    children: [
      {
        path: 'beneficiaries/beneficiaries-list',
        children: [
          {
            path: '',
            loadChildren: () => import('../beneficiaries/beneficiaries-list/beneficiaries.module').then(m => m.BeneficiariesPageModule)
          }
        ]
      },
      {
        path: 'beneficiaries/beneficiaries-detail',
        children: [
          {
            path: '',
            loadChildren: () => import('../beneficiaries/beneficiaries-detail/beneficiary.module').then(m => m.BeneficiaryPageModule)
          }
        ]
      },
    ]
  },
  {
    path: '',
    component: TabsPage,
    children: [
      {
        path: 'assistances/assistances-list',
        children: [
          {
            path: '',
            loadChildren: () => import('../assistances/assistances-list/assistances.module').then(m => m.AssistancesPageModule)
          }
        ]
      },
      {
        path: 'assistances/assistances-detail',
        children: [
          {
            path: '',
            loadChildren: () => import('../assistances/assistances-detail/assistance.module').then(m => m.AssistancePageModule)
          }
        ]
      },
      {
        path: 'assistancetypes/assistancetypes-list',
        children: [
          {
            path: '',
            loadChildren: () => import('../assistancetypes/assistancetypes-list/assistancetypes.module').then(m => m.AssistanceTypesPageModule)
          }
        ]
      },
      {
        path: 'churchs/churchs-list',
        children: [
          {
            path: '',
            loadChildren: () => import('../churchs/churchs-list/churchs.module').then(m => m.ChurchsPageModule)
          }
        ]
      },
    ]
  },
  // /app/ redirect
  {
    path: '',
    redirectTo: 'app/categories',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes), HttpClientModule],
  exports: [RouterModule],
  providers: [ ]
})
export class TabsPageRoutingModule {}
